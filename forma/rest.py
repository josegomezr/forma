#   Implementando un formador de URLs  
#   para los comandos HTTP REST enviados servidores
#   y eliminar codigo distractivos
#   Forma implicada del URL:

#   [protocol][:][//][usuario][:][clave][@][host][:][port][/][comando][/][bd][/][argumentos]

#   Ejemplo : 
#   forma.rest(<credenciales>, <comando rest>, [<argumentos>], [<motor de base de datos>])
#   forma.rest({'host':'diggi.duckdns.org','puerto':2480},'connect','sql/select from V'
#   rest se vale de ini.json """

import json
import os

aqui = os.path.join('forma','ini.json')

with open(aqui) as archivo:
    data = json.load(archivo)

def rest(p={}, c='', a='', motorDb='orientdb'):
    simbolo = data['simbolo']
    credencial = data['credencial']
    comando = data['servicio'][motorDb]['command']
    sintaxis = data['servicio'][motorDb]['syntax']
    pila = {}
    forma = ''

    ##> manejo de errores
    if not(c in comando):
        raise Exception("Comando no registrado en config")

    if isinstance(p, dict):
        credencial.update(p)
    else:
        ##> manejo de errores
        raise ValueError("Parametros no especificados")
        return None

    credencial['command'] = c

    pila.update(simbolo)
    pila.update(credencial)
    pila.update(p)

    #se puede mejorar; reducir cantidad de if.. etc
    #lo pongo asi para darme una idea de la cascada de acciones  
    #en el flujo de formacion del forma 
    #Verificar un modulo ya creado para trabajar lor URL
    #seguro los hay...

    ##> manejo de errores
    if not (credencial.get('host', False)):
        raise ValueError("'host' no especificado")

    if 'protocol' in credencial and credencial['protocol'] != '':
        forma += sintaxis['protocol']
    
    forma += sintaxis['host']

    if 'port' in credencial and credencial['port'] != 0:
        forma += sintaxis['port']
    
    forma += sintaxis['command']
    
    if 'name' in credencial and credencial['name'] !='':
        forma += sintaxis['name']
        """ pensar en recursividad de argumentos """

    if a:
        pila.update({'args':a})
        forma += sintaxis['args']

    # falta implementar manejo de errores. 

    return forma % pila